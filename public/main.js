 $('.multiple-items').slick({
      infinite: true,
      dots: true,
      slidesToShow: 4,
      slidesToScroll: 4,
	  variableWidth: false,
	  responsive: [
	{
	 breakpoint: 480,
      settings: {
        slidesToShow: 2,
		slidesToScroll: 2, 
		centerMode: false,
		centerPadding: '10px',
      }
    }
	]
    });


function setblock(p) {    
function updatePrice() {
    // Находим select по имени в DOM.
    let s = document.getElementsByName("prodType-" + p);
    let select = s[0];
    let textarea = document.getElementById("kol-" + p).value;
    let price = document.getElementById("constPrice-" + p).textContent;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    
        if (priceIndex >= 0) {
            if (prices.prodTypes[priceIndex] != 0) {
                price = (price + prices.prodTypes[priceIndex]) * textarea;
            } else {
                price *= textarea;
            };
        }
        
        console.log("debug 2 " + price);
        
         
        // Скрываем или показываем радиокнопки.
        let radioDiv = document.getElementById("radios-" + p);
        radioDiv.style.display = (select.value == "1" || select.value == "3" ? "none" : "block");
        // Смотрим какая товарная опция выбрана.
        let MyElementName = document.getElementById("radios-" + p);
        if (MyElementName.style.display != "none") {
            let radios = document.getElementsByName("prodOptions-" + p);
            radios.forEach(function (radio) {
                if (radio.checked) {
                    let optionPrice = prices.prodOptions[radio.value];
                    if (optionPrice !== undefined) {
                        price += optionPrice;
                    }
                }
            })
        };

         
        // Скрываем или показываем чекбоксы.
        let checkDiv = document.getElementById("checkboxes-" + p);
        checkDiv.style.display = (select.value == "1" || select.value == "2" ? "none" : "block");

        // Смотрим какие товарные свойства выбраны.
        let MyElement = document.getElementById("checkboxes-" + p);
        if (MyElement.style.display != "none") {
            let check = document.querySelector("#checkboxes-" + p);
            let checkboxes = check.querySelectorAll("input");
            checkboxes.forEach(function (checkbox) {
                if (checkbox.checked) {
                    let propPrice = prices.prodProperties[checkbox.name];
                    if (propPrice !== undefined) {
                        price = textarea * propPrice;
                    }
                }
            })
        };
        
        console.log("debug 1 " + price);
        let prodPrice = document.getElementById("prodPrice-" + p);
        prodPrice.innerHTML = price + "₽";
    }

    function getPrices() {
        return {
            prodTypes: [0, 400, 875],
            prodOptions: {
                option1: 50,
                option2: 75,
                option3: 200,
            },
            prodProperties: {
                prop1: 100,
                prop2: 10,
            }
        };
    }

    window.addEventListener('DOMContentLoaded', function (event) {
        // Скрываем радиокнопки.
        let radioDiv = document.getElementById("radios-" + p);
        radioDiv.style.display = "none";

        // Находим select по имени в DOM.
        let s = document.getElementsByName("prodType-" + p);
        let select = s[0];
        // Назначаем обработчик на изменение select.
        select.addEventListener("change", function (event) {
            let target = event.target;
            console.log("выбрал селект " + target.value);
            updatePrice();
        });
        let textarea = document.getElementById("kol-" + p);
        textarea.addEventListener("change", function (event) {
            let f = event.target;
            console.log("количество " + f.value);
            updatePrice();
        });
        // Назначаем обработчик радиокнопок.  
        let radios = document.getElementsByName("prodOptions-" + p);
        radios.forEach(function (radio) {
            radio.addEventListener("change", function (event) {
                let r = event.target;
                console.log("выбрал радио " + r.value);
                updatePrice();
            });
        });
        // Назначаем обработчик чекбоксов.  
        let check = document.querySelector("#checkboxes-" + p);
        let checkboxes = check.querySelectorAll("input");
        checkboxes.forEach(function (checkbox) {
            checkbox.addEventListener("change", function (event) {
                let c = event.target;
                console.log(c.name);
                console.log(c.value);
                updatePrice();
            });
        });
        updatePrice();
    })
};

 
function setBLocks() {
    var inputs = document.getElementById("PRODUCT_list");
    var input = inputs.children;
    for (var i = 0; i < input.length - 1; i++) {
        var pol = input[i].children[5].childNodes[1].childNodes[7];
        console.log(pol);
        var l = String(i);
        setblock(l);
    }
}

setBLocks();
